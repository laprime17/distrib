from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response
from django.http import HttpResponse
import json 
import numpy as np
import matplotlib.pyplot as plt

def formulario(request):
	return render(
    request,
    'views/home.html',
    context={'num_books':1}
    )

def weib(x,n,a):
	return (a / n) * (x / n)**(a - 1) * np.exp(-(x / n)**a)

def get_data_weibull(request):
	
	par_forma = request.GET['forma']
	par_escala = request.GET['escala']
	par_cant_datos = request.GET['cant']

	a = float(par_forma)
	par_escala = float(par_escala)
	par_cant_datos = int(par_cant_datos)

	s= np.random.weibull(a, par_cant_datos)
	x = np.arange(1,par_cant_datos)/50.

	count, bins, ignored = plt.hist(np.random.weibull(5.,par_cant_datos))
	x = np.arange(1,par_cant_datos)/50.
	scale = count.max()/weib(x, par_escala,a).max()
	plt.plot(x, weib(x, par_escala,a)*scale)

	ax = plt.gca() 
	line = ax.lines[0]
	arreglo = line.get_xydata()
	tamanio = len(arreglo)
	i = 0
	A = []

	while i<tamanio:
		A.append([round(arreglo[i][0],2),round(arreglo[i][1],2)])
		i=i+1

	return HttpResponse(json.dumps(A), content_type = "application/json")



#plt.show()

